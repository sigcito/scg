<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard (Request $request){
        //dd($request->query('title', 'HolaRico')); Test request 
        return view ('dashboard', [
            'title' => 'rico'
        ]);
    }
}
